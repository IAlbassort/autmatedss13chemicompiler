# Outdated

As of Nov 2024, the code no longer works to a degree which I would consider acceptable. I would need to write this form the ground up to make it owrk as it did before with the modern chemistry, as, things have changed.

# Credits
This work uses a chemicompiler made by Slugo
their project: https://112358sam.github.io/ChemiAssembler/
all of ./compiler was written by them, and modified by me to work with node.

# What is this?
In the game "SS13", specifically, a branch called "GoonStation" there is a machine which can do chemistry. Chemistry is done in this game by mixing chemicals. There is a machine in this game which can automatically mix chemicals to achieve specific ends. The difficulty is, you need to write the code to achieve it, manually. This is the isuse this project was intended to solve

The Philosophy was: You enter the chemnicals yo uwant it, the code would create a list of inputs, that would be re-arranged into the desired output.

# Well what CAN it DO?
Its not perfect. There are a ton of edge cases. Chems in goonstation strive to be unique, and all depth to the game's mechanics. This creates ton of strange behavior... some chems just cant be done.
It can create most chems with very little flaws. just enter them in and it should give you a perfect output.
Put meth 100u in, it'll yield you a 75 pill and a 26 pill (when working with ratios, its as close as you can get).
We round up, then down. Its better to have more chems in the storage then less.

# What CANT it do?
Though its flawed, not perfect. When it fails, it fails hard. The output will look nothing like the expected output and if you're doing pyrotechnical chemicals, a large chance of an explosion. If you're working with a lot of them, all chemistry.
Currently, without any additional work... any chems which violate conservation of mass have a pretty high likelihood of not working (Thermite, for example.). This is a simple problem with a pretty simple fix; however im kinda burnt out to work on this anymore to fix it currently.
Currently does not cool down chems before output. This can lead to your meth burning you on output. Mostly isn't a big problem... provided nobody teleports any bread.

Sometimes, the recipes for chems mention chems which don't exist. This is because shorthand is sometimes used. This can create problems all over the place but its a blackhole of over 400 recipes to write code for edgecases.

Crafty any secret chems! I want to add a feature where you add your own secret chem recipes but that currently is not in the codebase.


# Well, thats cool. How do I use it?
No ui currently.

first you must download the recipes
```
node download.js
```
we're off to go!

go to line 704 (as of writing) and you'll have the chems.
by default its `["Liquid Dark Matter", 250], ["Meth", 200],  ["Smoke Powder", 50]`
this will output 500u total. This is a bomb which will ignite, then get you high...
You add your own values like
`[["Pure Love", 50], ["Rajaijah", 30], ["Fluorosulfuric Acid", 10], ["Smoke Powder", 10]]`
```node main.js```
the output happens in 3 stages
1. The input chems
for example:
`
Carbon=26;Ethanol=12;Iron=41;Oxygen=13
beaker {
  chems: [
    [ 'Carbon', 26 ],
    [ 'Ethanol', 12 ],
    [ 'Iron', 41 ],
    [ 'Oxygen', 13 ],
    [ 'Welding Fuel', 8 ]
  ],
  max: 100,
  beakernumber: 3,
  temp: 293.15
}
`
2. The commands
for example:
`
iso 12, 1, 1, 9
iso 4, 2, 2, 9
mov 12, 9, 1
iso 8, 2, 2, 9
iso 8, 1, 1, 9
iso 8, 5, 3, 9
mov 8, 9, 1
mov 12, 9, 2
mov 4, 9, 3
iso 12, 2, 1, 9
...
`
3. The precompiled output (manual compilation https://112358sam.github.io/ChemiAssembler/)
for example:
`++++++++++++'>+}>+++++++++)<#>>++++'>++}#<<<<'>>}<)@>>>>++++++++'<}<<)>>#<<<}#>>>>>+++}>+++++#<<<<<}<)@<'>>>>)@<'>>>)@<<<<<<'>}>)>>#>>}<<#<<}>>>>>>)+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'$<<<<<<<<'>)@>>>>>)@>>>+++++++++++++++++++++++++++++++++'<<<}<<<<)>>#<<<}#`...
