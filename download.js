const request = require("request");
const url = "https://wiki.ss13.co/index.php?title=Chemicals&oldid=48701"; // input your url here
const parser = require("node-html-parser");
const fs = require("fs");

final = {};
class Recipe {
  constructor(name, recipe, ignition) {
    this.name = name;
    this.recipe = recipe;
    this.ignition = ignition;
  }
}

class ingredients {
  constructor(chemstring, temp, yields, chemarray = [], name, ignition = 0) {
    this.chemstring = chemstring;
    this.temp = temp;
    this.yields = yields;
    this.chemarray = chemarray;
    this.name = name;
    this.ignition = ignition;
  }
}
let identifiers = ["->", "Secret!", "N/A"];
//gets the digit between the () | eg '(1)' => 1
let regExp = /\(([^)]+)\)/;
let getTemp = /\d{3}/;
function alter(input, out) {
  //gets the name of the ingredient separately from its amount '(1) charcoal' => 'charcoal'
  let base = input.split(") ")[1];
  //gets the recipe portions
  let number = regExp.exec(input)[1];
  if (base.includes("@")) {
    base = base.split("@")[0].trim();
  }
  if (base.includes(" (")) {
    base = base.split(" (")[0];
  }
  if (base.includes(")")) {
    base = base.split(")")[0];
  } //formats it how ss13 likes. ie charcoal=10;vodka=20; ...
  out.push([base, Number(number)]);
  return base + "=" + number + ";";
}
async function download() {
  let failed = [];
  let out = await new Promise((resolve) => {
    request(url, function (err, res, body) {
      let found = [];
      let output = [];
      const origin = parser.parse(body).getElementsByTagName("table");

      for (let node in origin) {
        //gets each table
        let bodies = origin[node].getElementsByTagName("tbody");
        for (let y in bodies) {
          //get each tables subcontents. idrk html, idk
          let tableEntry = bodies[y].getElementsByTagName("tr");

          for (let z in tableEntry) {
            let holder = [];
            //this is the first place you can find the recipe name. For convenience, i grab it here
            let chemparts = tableEntry[z].getElementsByTagName("td");
            let id = tableEntry[z]["id"];

            for (var x in chemparts) {
              //here is where all the contents per recipe is contained
              let text = chemparts[x]["text"];
              //but i only want the ingredients
              if (text == "") {
                continue;
              }
              //this puts hints in because not all chems can be made
              if (id.includes("_Hint")) {
                holder.push(chemparts[x]["text"]);
                holder.push(chemparts[Number(x) + 2]["text"]);
                break;
              }

              if (x == 0) {
                holder.push(text.replace("\n", ""));
              }
              for (x in identifiers) {
                if (text.includes(identifiers[x]) && text.length >= 5) {
                  holder.push(text.replace("_", " ").replace("\n", ""));
                  try {
                    let explosion = chemparts[Number(x) + 4]["text"];
                    if (explosion.includes(" K")) {
                      if (explosion.length < 30) {
                        holder.push(Number(getTemp.exec(explosion)[0]));
                      }
                    }
                  } catch {}
                  break;
                }
              }
              if (holder.includes(text)) {
                continue;
              }
              //JS cant compare between arrays because pointers, return individual objects and thus can never be equal
              //having a second list is just for convenience
            }
            if (
              holder.length == 2 ||
              (holder.length == 3) & !found.includes(holder[1])
            ) {
              found.push(holder[1]);
              output.push(holder);
              if (holder.length == 3) {
              }
            }
          }
        }
      }
      let changed;
      for (x in output) {
        //sometimes recipes refer to chems which "Dont exist"
        //This solves the most obvious ones.
        let name = output[x][0];
        if (name.includes("Diphenhydramine")) {
          name = "Diphenhydramine";
        }
        correctednames = {
          "Stabilizing Agent(Stabilizer)": "Stabilizing Agent",
          Methamphetamine: "Meth",
        };
        //little spaghetti because js is odd.
        if (Object.keys(correctednames).includes(name)) {
          name = correctednames[name];
        }
        let nonCompat = [
          "Foamed Metal",
          "Magnesium Explosion",
          "Life",
          "Flaptonium",
        ];
        if (nonCompat.includes(name)) {
          continue;
        }
        let recipe = output[x][1];
        let ors;

        if (recipe.includes("OR")) {
          ors = recipe.split("OR");
        } else {
          ors = [recipe];
        }
        let ignition = 0;
        if (output[x][2] != undefined) {
          if (typeof output[x][2] != typeof 1) {
            continue;
          }
          ignition = output[x][2];
        }

        let recipes = [];
        for (let eachrecipe of ors) {
          if (!eachrecipe.includes("->")) {
            recipes.push(new ingredients(eachrecipe, -1, 0, undefined, name));
            continue;
          }

          let temperature = -1;
          let yields;
          try {
            yields = regExp.exec(eachrecipe.split("->")[1])[1];
          } catch (e) {
            failed.push(name);
            continue;
          }
          //splits the recipe down the yield sign, and organizes it by ingredient
          changed = eachrecipe.split(" -> ")[0].split("+");

          //defines a function to get the last item, it looks pretty <3
          changed.last = function () {
            return changed[changed.length - 1];
          };

          //@ is a temperature signal.
          //if the last chemical reagent has a temperature mark that means that it has to be conducted at a set temp.
          if (changed.last().includes("@")) {
            //regExp go brr... Finds any 3 digit number and sets it as the temp. Seems like a bad idea but i doubt it'll be bad
            temperature = Number(getTemp.exec(eachrecipe));
            //output[x][0] = output[x][0] + " @" + split[1].split(" -> ")[0]
          }
          //an array of chems with their name, and portion
          let outchems = [];
          try {
            changed = changed
              .map((x) => x.trim())
              .map((x) => alter(x, outchems))
              .join("");
          } catch (e) {
            failed.push(name);
            continue;
          }
          if (temperature != -1) {
            changed = changed.split("@")[0].trim();
          }

          if (changed.includes("(=1;Stabilizing Agent)=1;")) {
            changed = changed.split(" (=1;Stabilizing Agent)=1;")[0] +=
              ";stabilizing Agent=1;";
          }
          let newIngredient;
          try {
            newIngredient = new ingredients(
              changed,
              temperature,
              parseInt(yields),
              outchems,
              name,
              ignition,
            );
          } catch (e) {
            failed.push(name);
            continue;
          }
          recipes.push(newIngredient);
        }
        final[name] = new Recipe(name, recipes, ignition);
      }
      console.log("The following failed:");
      console.log(failed);
      resolve(final);
    });
  });
  fs.writeFileSync("out.json", JSON.stringify(out));
}
download();
exports.ingredients = ingredients;
